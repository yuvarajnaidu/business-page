package com.business.backend.controller;


import com.business.backend.model.ApiResponse;
import com.business.backend.model.PaymentIntentModel;
import com.business.backend.model.PersonalDetail;
import com.business.backend.model.StripeRequest;
import com.business.backend.service.OrderService;
import com.business.backend.service.PaymentIntentService;
import com.business.backend.service.StripeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("api/checkout/")
public class CheckoutController {

    @Autowired
    StripeService stripeService;

    @Autowired
    PaymentIntentService paymentIntentService;

    @Autowired
    OrderService orderService;

    @RequestMapping(value = "getStripeResponse",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    ApiResponse saveUser(HttpServletRequest httpServletRequest) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            apiResponse.setObject(stripeService.requestPaymentToken(httpServletRequest));
            apiResponse.setSuccessful(true);
            return apiResponse;
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }

    @RequestMapping(value = "saveShippingDetails",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ApiResponse saveShippingDetails(@RequestBody PersonalDetail personalDetail, HttpServletRequest httpServletRequest) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            orderService.savePersonalDetail(personalDetail, httpServletRequest);
            apiResponse.setSuccessful(true);
            return apiResponse;
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }

    @RequestMapping(value = "validatePaymentIntent",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ApiResponse validatePaymentIntent(@RequestBody PaymentIntentModel paymentIntentModel, HttpServletRequest httpServletRequest) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            stripeService.validatePaymentIntent(paymentIntentModel, httpServletRequest);
            apiResponse.setSuccessful(true);
            return apiResponse;
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }
}
