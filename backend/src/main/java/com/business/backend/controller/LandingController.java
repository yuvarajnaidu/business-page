package com.business.backend.controller;

import com.business.backend.Enum.PaymentIntentStatus;
import com.business.backend.configuration.JwtConfiguration;
import com.business.backend.model.PaymentIntentModel;
import com.business.backend.service.StripeService;
import com.business.backend.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin(value = {"http://localhost:8080"})
@Controller
public class LandingController {

    @Autowired
    StripeService stripeService;

    @Autowired
    JwtConfiguration jwtConfiguration;
    boolean success;


    @RequestMapping(path = "/")
    public String landingPage(HttpServletResponse httpServletResponse) {
        final Cookie cookie = new Cookie("cott8n-x-token", "");
        cookie.setDomain("");
        cookie.setSecure(false);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
        return "landing_index";
    }

    @RequestMapping(path = "/checkout")
    public String checkOut(Model model, @RequestParam(value = "payment_intent") String paymentIntent, @RequestParam(value = "payment_intent_client_secret") String paymentIntentClientSecret, @RequestParam(value = "redirect_status") String redirectStatus, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        PaymentIntentModel paymentIntentModel = new PaymentIntentModel();
        paymentIntentModel.setPaymentIntent(paymentIntent);
        paymentIntentModel.setPaymentIntentClientSecret(paymentIntentClientSecret);
        paymentIntentModel.setRedirectStatus(redirectStatus);
        stripeService.validatePaymentIntent(paymentIntentModel, httpServletRequest);
        if (paymentIntentModel.getRedirectStatus().equals(PaymentIntentStatus.SUCCEEDED.getStatus())) {
            success=true;
        } else if (paymentIntentModel.getRedirectStatus().equals(PaymentIntentStatus.FAILED.getStatus())) {
            success=false;

        }
        final Cookie cookie = new Cookie("cott8n-x-token", "");
        cookie.setDomain("");
        cookie.setSecure(false);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        httpServletResponse.addCookie(cookie);
        return "redirect:/?success="+success;
    }

}
