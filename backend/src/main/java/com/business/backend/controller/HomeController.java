package com.business.backend.controller;

import com.business.backend.capturedModel.CapturedProduct;
import com.business.backend.configuration.JwtConfiguration;
import com.business.backend.model.*;
import com.business.backend.service.OrderService;
import com.business.backend.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("api/home/")
public class HomeController {

    private Logger logger = LoggerFactory.getLogger(HomeController.class);


    @Autowired
    ProductService productService;

    @Autowired
    OrderService orderService;

    @Autowired
    private JwtConfiguration jwtConfiguration;

    @RequestMapping(value = "addProduct", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ApiResponse saveUser(@RequestBody Product product) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            apiResponse.setObject(productService.saveProduct(product));
            apiResponse.setSuccessful(true);
            return apiResponse;
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }

    @RequestMapping(value = "getAllProducts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ApiResponse getAllProducts() throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            apiResponse.setObject(productService.getAllProducts());
            apiResponse.setSuccessful(true);
            return apiResponse;
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }

    @RequestMapping(value = "saveCart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ApiResponse saveCart(@RequestBody List<CapturedProduct> capturedProductList) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            apiResponse = orderService.saveOrder(capturedProductList);
            return apiResponse;
        } catch (Exception e) {
            apiResponse.setSuccessful(false);
            apiResponse.setMessage(e.getMessage());
            e.printStackTrace();
        }
        return apiResponse;
    }


    @RequestMapping(value = "authenticateCheckout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    void authenticateCheckout(@RequestBody PersonalDetail personalDetail, HttpServletResponse httpServletResponse) throws Exception {
        try {
            success(personalDetail, httpServletResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void success(PersonalDetail personalDetail, HttpServletResponse response) throws Exception {
        logger.debug(String.format("Personal session %s aunthenticate successfully!", personalDetail.getUserEmail()));

        String token = generateJwtToken(personalDetail);
        if (null != token) {
            final Cookie cookie = new Cookie("cott8n-x-token", jwtConfiguration.getPrefix().trim() + token);
            cookie.setDomain("");
            cookie.setSecure(false);
            cookie.setHttpOnly(true);
            cookie.setMaxAge(jwtConfiguration.getExpiration() * 5000);
            cookie.setPath("/");
            response.addCookie(cookie);

            response.setContentType("application/json");
            final ObjectNode jsonResponse = JsonNodeFactory.instance.objectNode();
            jsonResponse.put("token", jwtConfiguration.getPrefix() + token);
            response.getWriter().print(jsonResponse.toString());
            response.getWriter().flush();

//            response.setStatus(HttpStatus.OK.value());
//            response.setContentType("application/json");
//            final ObjectNode jsonResponse = JsonNodeFactory.instance.objectNode();
//            jsonResponse.put("token", jwtConfiguration.getPrefix() + token);
//            ApiResponse apiResponse = new ApiResponse();
//            apiResponse.setSuccessful(true);
//            apiResponse.setObject(jsonResponse);
//            response.getWriter().print(new ObjectMapper().writeValueAsString(apiResponse));
//            response.getWriter().flush();
        } else {
            unsuccessful(response);
        }
    }

    private void unsuccessful(HttpServletResponse response) throws Exception {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        final ObjectNode jsonResponse = JsonNodeFactory.instance.objectNode();
        jsonResponse.put("authenticated", Boolean.FALSE.booleanValue());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccessful(false);
        apiResponse.setObject(jsonResponse);
        response.getWriter().print(new ObjectMapper().writeValueAsString(apiResponse));
        response.getWriter().flush();
    }

    private String generateJwtToken(PersonalDetail personalDetail) throws Exception {
        String token;

        long now = System.currentTimeMillis();
        token = Jwts.builder()
                .setSubject(personalDetail.getId())
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + jwtConfiguration.getExpiration() * 5000))  // in milliseconds
                .signWith(SignatureAlgorithm.HS512, jwtConfiguration.getSecret().getBytes())
                .compact();

        return token;
    }


//    @RequestMapping(value = "saveItemCarts",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    ApiResponse saveOrder(@RequestBody List<CartItem> cartItemList) throws Exception {
//        ApiResponse apiResponse = new ApiResponse();
//        try {
//            apiResponse.setObject(orderService.saveItemCart(cartItemList));
//            apiResponse.setSuccessful(true);
//            return apiResponse;
//        } catch (Exception e) {
//            e.printStackTrace();
//            apiResponse.setSuccessful(false);
//            return apiResponse;
//        }
//    }

}
