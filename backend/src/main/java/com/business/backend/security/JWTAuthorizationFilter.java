package com.business.backend.security;

import com.business.backend.configuration.JwtConfiguration;
import com.business.backend.model.ApiResponse;
import com.business.backend.model.PersonalDetail;
import com.business.backend.service.GetTokenService;
import com.business.backend.service.OrderService;
import com.business.backend.service.impl.SecurityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authManager, JwtConfiguration jwtConfiguration, JwtTokenProvider tokenProvider, SecurityService securityService, OrderService orderService, GetTokenService getTokenService) {
        super(authManager);
        this.jwtConfiguration = jwtConfiguration;
        this.tokenProvider = tokenProvider;
        this.securityService = securityService;
        this.orderService = orderService;
        this.getTokenService = getTokenService;
    }

    private JwtTokenProvider tokenProvider;

    private SecurityService securityService;

    private OrderService orderService;

    private JwtConfiguration jwtConfiguration;

    private GetTokenService getTokenService;


    private static final Logger logger = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
/*
        boolean thisUrlWasAuthorized = false;
*/
        try {
            String jwt = null;
            String cookieValue = getTokenService.getTokenFromCookie(request);

            if (!StringUtils.isEmpty(cookieValue))
                jwt = getJwtFromCookie(cookieValue);


            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                String id = tokenProvider.getIdFromToken(jwt);
                PersonalDetail personalDetail = securityService.loadPersonalDetailById(id);

                if (null == personalDetail)
                    throw new Exception("invalid jwt token");


                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(personalDetail, null, null);
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
/*
                thisUrlWasAuthorized = true;
*/
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);

       /* if (thisUrlWasAuthorized)
            logger.info("i have the response for "+request.getRequestURI());*/
    }

    public String getJwtFromCookie(String token) throws Exception {
        String jwt = null;
        if (StringUtils.hasText(token) && token.startsWith(jwtConfiguration.getPrefix())) {
            jwt = token.substring(jwtConfiguration.getPrefix().length());
        }
        return jwt;
    }
}