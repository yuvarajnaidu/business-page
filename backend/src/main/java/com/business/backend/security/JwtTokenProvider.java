package com.business.backend.security;


import com.business.backend.configuration.JwtConfiguration;
import com.business.backend.model.PersonalDetail;
import com.business.backend.utils.Constants;
import com.stripe.model.Person;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Autowired
    private JwtConfiguration jwtConfiguration;

    @Value("${nedas.runconfig.lanmode:false}")
    private boolean lanMode;

    public String generateToken(PersonalDetail personalDetail) {

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtConfiguration.getExpiration()*1000);

        return Jwts.builder()
                .setSubject(personalDetail.getId())
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtConfiguration.getSecret())
                .compact();
    }

    public String getIdFromToken(String token) throws Exception {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtConfiguration.getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }

    Object getClaimFromToken(String claim, String token) throws Exception {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtConfiguration.getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();

        return claims.get(claim);
    }

    boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtConfiguration.getSecret().getBytes()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }

    public String generatePublicJwtToken(List<GrantedAuthority> list) throws Exception {
        String token;
        long now = System.currentTimeMillis();
        token = Jwts.builder()
                .setSubject("ANONYMOUS_USER")
                .claim(Constants.CLAIM_AUTHORITIES, list.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + jwtConfiguration.getExpiration() * 1000))  // in milliseconds
                .signWith(SignatureAlgorithm.HS512, jwtConfiguration.getSecret().getBytes())
                .compact();

        return jwtConfiguration.getPrefix().trim()+token;
    }

    public void setPublicJwtToken(HttpServletResponse httpServletResponse) throws Exception {
        final Cookie tokenCookie = new Cookie(Constants.PUBLIC_TOKEN, generatePublicJwtToken(Collections.EMPTY_LIST));
        tokenCookie.setDomain("");
        if (!lanMode) {
            tokenCookie.setSecure(true);
            tokenCookie.setHttpOnly(true);
        }
        tokenCookie.setMaxAge(60 * 60);
        tokenCookie.setPath("/");
        httpServletResponse.addCookie(tokenCookie);
    }
}