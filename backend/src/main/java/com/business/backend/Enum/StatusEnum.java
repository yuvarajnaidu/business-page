package com.business.backend.Enum;

public enum StatusEnum {

    PAYMENT_REQUEST("PAYMENT_REQUEST"),
    CAPTURED_PERSONAL_DATA_PAYMENT_PROCESS("CAPTURED_PERSONAL_DATA_PAYMENT_PROCESS"),
    PAYMENT_SUCCESS("PAYMENT_SUCCESS"),
    PAYMENT_FAIL("PAYMENT_FAIL"),
    ORDER_PLACED("ORDER_PLACED");

    private String status;

    StatusEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
