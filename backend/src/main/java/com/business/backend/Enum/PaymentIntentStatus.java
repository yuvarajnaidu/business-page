package com.business.backend.Enum;

public enum PaymentIntentStatus {
    SUCCEEDED("succeeded"),
    FAILED("failed");

    private String status;

    PaymentIntentStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
