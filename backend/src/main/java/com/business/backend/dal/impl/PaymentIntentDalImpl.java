package com.business.backend.dal.impl;
import com.business.backend.dal.PaymentIntentDal;
import com.business.backend.model.PaymentIntentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentIntentDalImpl implements PaymentIntentDal {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public PaymentIntentDalImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public PaymentIntentModel save(PaymentIntentModel paymentIntentModel) throws Exception {
        return mongoTemplate.save(paymentIntentModel);
    }

    @Override
    public List<PaymentIntentModel> getAllPaymentIntents() throws Exception {
        return mongoTemplate.findAll(PaymentIntentModel.class);
    }
    @Override
    public List<PaymentIntentModel> findPaymentIntentsByPersonalId(String personalId) throws Exception {
        Query query = new Query();
        query.addCriteria(Criteria.where("personal_detail_id").is(personalId));
        return mongoTemplate.find(query, PaymentIntentModel.class);
    }


}
