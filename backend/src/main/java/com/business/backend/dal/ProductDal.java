package com.business.backend.dal;

import com.business.backend.model.Product;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public interface ProductDal {

    List<Product> getAllProducts() throws Exception;

    Product update(Query findByQuery, Update updateData) throws Exception;
    List<Product> findProductByAvailable(boolean available) throws Exception;

    Product findProductById(String id) throws Exception;
    void save(Product product) throws Exception;
    void saveAll(List<Product> products) throws Exception;


}
