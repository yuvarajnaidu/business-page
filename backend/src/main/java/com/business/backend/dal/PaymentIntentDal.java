package com.business.backend.dal;

import com.business.backend.model.PaymentIntentModel;

import java.util.List;

public interface PaymentIntentDal {

    List<PaymentIntentModel> getAllPaymentIntents() throws Exception;

    List<PaymentIntentModel> findPaymentIntentsByPersonalId(String personalId) throws Exception;
    PaymentIntentModel save(PaymentIntentModel paymentIntentModel) throws Exception;


}
