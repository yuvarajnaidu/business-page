package com.business.backend.dal;

import com.business.backend.model.PersonalDetail;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public interface PersonalDetailDal {

    List<PersonalDetail> getAllPersonalDetails() throws Exception;

    PersonalDetail update(Query findByQuery, Update updateData) throws Exception;

    PersonalDetail findPersonalDetailById(String id) throws Exception;
    PersonalDetail findPersonalDetailByToken(String token) throws Exception;
    PersonalDetail save(PersonalDetail personalDetail) throws Exception;
    void saveAll(List<PersonalDetail> personalDetails) throws Exception;


}
