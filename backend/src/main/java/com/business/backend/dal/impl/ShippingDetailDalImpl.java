package com.business.backend.dal.impl;
import com.business.backend.dal.ShippingDetailDal;
import com.business.backend.model.ShippingDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ShippingDetailDalImpl implements ShippingDetailDal {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public ShippingDetailDalImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void save(ShippingDetail shippingDetail) throws Exception {
        mongoTemplate.save(shippingDetail);
    }

    public void saveAll(List<ShippingDetail> shippingDetails) throws Exception {
        for(ShippingDetail shippingDetail: shippingDetails)
            save(shippingDetail);
    }

    @Override
    public List<ShippingDetail> getAllProducts() throws Exception {
        return mongoTemplate.findAll(ShippingDetail.class);
    }

    @Override
    public ShippingDetail update(Query findByQuery, Update updateData) throws Exception {
        return mongoTemplate.findAndModify(findByQuery, updateData,new FindAndModifyOptions().returnNew(true), ShippingDetail.class);
    }

    @Override
    public ShippingDetail findShippingDetailById(String id) throws Exception {
        return mongoTemplate.findById(id,ShippingDetail.class);
    }


}
