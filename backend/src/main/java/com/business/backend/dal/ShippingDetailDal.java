package com.business.backend.dal;

import com.business.backend.model.ShippingDetail;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public interface ShippingDetailDal {

    List<ShippingDetail> getAllProducts() throws Exception;

    ShippingDetail update(Query findByQuery, Update updateData) throws Exception;

    ShippingDetail findShippingDetailById(String id) throws Exception;
    void save(ShippingDetail shippingDetail) throws Exception;
    void saveAll(List<ShippingDetail> shippingDetails) throws Exception;


}
