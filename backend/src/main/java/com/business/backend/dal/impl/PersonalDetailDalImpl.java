package com.business.backend.dal.impl;
import com.business.backend.dal.PersonalDetailDal;
import com.business.backend.model.PersonalDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonalDetailDalImpl implements PersonalDetailDal {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public PersonalDetailDalImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public PersonalDetail save(PersonalDetail personalDetail) throws Exception {
        return mongoTemplate.save(personalDetail);
    }

    public void saveAll(List<PersonalDetail> personalDetails) throws Exception {
        for(PersonalDetail personalDetail: personalDetails)
            save(personalDetail);
    }

    @Override
    public List<PersonalDetail> getAllPersonalDetails() throws Exception {
        return mongoTemplate.findAll(PersonalDetail.class);
    }

    @Override
    public PersonalDetail update(Query findByQuery, Update updateData) throws Exception {
        return mongoTemplate.findAndModify(findByQuery, updateData,new FindAndModifyOptions().returnNew(true), PersonalDetail.class);
    }

    @Override
    public PersonalDetail findPersonalDetailByToken(String token) throws Exception {
        Query query = new Query();
        query.addCriteria(Criteria.where("token").is(token));
        return mongoTemplate.findOne(query, PersonalDetail.class);
    }

    @Override
    public PersonalDetail findPersonalDetailById(String id) throws Exception {
        return mongoTemplate.findById(id,PersonalDetail.class);
    }


}
