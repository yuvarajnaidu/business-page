package com.business.backend.dal.impl;



import com.business.backend.dal.ProductDal;
import com.business.backend.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductDalImpl implements ProductDal {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public ProductDalImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void save(Product product) throws Exception {
        mongoTemplate.save(product);
    }

    public void saveAll(List<Product> products) throws Exception {
        for(Product product: products)
            save(product);
    }

    @Override
    public List<Product> getAllProducts() throws Exception {
        return mongoTemplate.findAll(Product.class);
    }

    @Override
    public Product update(Query findByQuery, Update updateData) throws Exception {
        return mongoTemplate.findAndModify(findByQuery, updateData,new FindAndModifyOptions().returnNew(true), Product.class);
    }

    @Override
    public List<Product> findProductByAvailable(boolean available) throws Exception {
        Query query = new Query(Criteria
                .where("available").is(available));
        return mongoTemplate.find(query,Product.class);
    }

    @Override
    public Product findProductById(String id) throws Exception {
        return mongoTemplate.findById(id,Product.class);
    }


}
