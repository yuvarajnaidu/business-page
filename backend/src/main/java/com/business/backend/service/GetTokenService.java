package com.business.backend.service;

import javax.servlet.http.HttpServletRequest;

public interface GetTokenService {


    String getTokenFromCookie(HttpServletRequest httpServletRequest) throws Exception;
    String getJwtFromCookie(String token) throws Exception;



}
