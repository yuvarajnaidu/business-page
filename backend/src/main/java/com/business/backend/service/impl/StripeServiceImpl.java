package com.business.backend.service.impl;

import com.business.backend.Enum.PaymentIntentStatus;
import com.business.backend.Enum.StatusEnum;
import com.business.backend.dal.PersonalDetailDal;
import com.business.backend.model.PaymentIntentModel;
import com.business.backend.model.PersonalDetail;
import com.business.backend.model.Product;
import com.business.backend.model.StripeRequest;
import com.business.backend.service.GetTokenService;
import com.business.backend.service.OrderService;
import com.business.backend.service.ProductService;
import com.business.backend.service.StripeService;
import com.stripe.Stripe;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class StripeServiceImpl implements StripeService {

    @Value("${stripe.api.key}")
    private String apiKey;

    @Autowired
    OrderService orderService;

    @Autowired
    PersonalDetailDal personalDetailDal;

    @Autowired
    GetTokenService getTokenService;

    @Override
    public StripeRequest requestPaymentToken(HttpServletRequest httpServletRequest) throws Exception {
        Stripe.apiKey = apiKey;
        String token = getTokenService.getTokenFromCookie(httpServletRequest);

        PersonalDetail personalDetail = personalDetailDal.findPersonalDetailById(getTokenService.getJwtFromCookie(token));

        PaymentIntentCreateParams params =
                PaymentIntentCreateParams.builder()
                        .setAmount(personalDetail.getTotalPrice())
                        .setCurrency("myr")
                        .addPaymentMethodType("fpx")
                        .build();

        PaymentIntent paymentIntent = PaymentIntent.create(params);
        StripeRequest stripeRequest = new StripeRequest();
        stripeRequest.setClient_secret(paymentIntent.getClientSecret());
        stripeRequest.setStatus(paymentIntent.getStatus());
        stripeRequest.setAmount(paymentIntent.getAmount());

        personalDetail.setStripeRequest(stripeRequest);
        personalDetail.setToken(token);
        personalDetail.setCurrentStatus(StatusEnum.PAYMENT_REQUEST.getStatus());
        orderService.savePersonalDetail(personalDetail);

        return stripeRequest;

    }

    @Override
    public void validatePaymentIntent(PaymentIntentModel paymentIntentModel, HttpServletRequest httpServletRequest) throws Exception {
        String token = getTokenService.getTokenFromCookie(httpServletRequest);

        PersonalDetail personalDetail = personalDetailDal.findPersonalDetailById(getTokenService.getJwtFromCookie(token));

        personalDetail.setPaymentIntentModel(paymentIntentModel);
        if (paymentIntentModel.getRedirectStatus().equals(PaymentIntentStatus.SUCCEEDED.getStatus())) {
            personalDetail.setCurrentStatus(StatusEnum.PAYMENT_SUCCESS.getStatus());
        } else if (paymentIntentModel.getRedirectStatus().equals(PaymentIntentStatus.FAILED.getStatus())) {
            personalDetail.setCurrentStatus(StatusEnum.PAYMENT_FAIL.getStatus());
        }
        orderService.savePersonalDetail(personalDetail);
    }
}
