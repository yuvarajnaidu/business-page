package com.business.backend.service;

import com.business.backend.capturedModel.CapturedProduct;
import com.business.backend.model.ApiResponse;
import com.business.backend.model.PersonalDetail;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OrderService {

    ApiResponse saveOrder(List<CapturedProduct> capturedProductList) throws Exception;
    void savePersonalDetail(PersonalDetail personalDetail) throws Exception;
    PersonalDetail findPersonalDetailById(String id) throws Exception;
    void savePersonalDetail(PersonalDetail personalDetail, HttpServletRequest httpServletRequest) throws Exception;
}
