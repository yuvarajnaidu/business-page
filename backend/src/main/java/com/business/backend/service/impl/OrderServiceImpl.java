package com.business.backend.service.impl;

import com.business.backend.Enum.StatusEnum;
import com.business.backend.capturedModel.CapturedProduct;
import com.business.backend.configuration.JwtConfiguration;
import com.business.backend.dal.PersonalDetailDal;
import com.business.backend.dal.ProductDal;
import com.business.backend.dal.ShippingDetailDal;
import com.business.backend.model.ApiResponse;
import com.business.backend.model.PersonalDetail;
import com.business.backend.model.Product;
import com.business.backend.security.JwtTokenProvider;
import com.business.backend.service.GetTokenService;
import com.business.backend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {


    @Autowired
    ShippingDetailDal shippingDetailDal;

    @Autowired
    PersonalDetailDal personalDetailDal;

    @Autowired
    ProductDal productDal;

    @Autowired
    private GetTokenService getTokenService;


    @Override
    public ApiResponse saveOrder(List<CapturedProduct> capturedProductList) throws Exception {
        PersonalDetail personalDetail = new PersonalDetail();
        List<String> expiredProductList = new ArrayList<>();
        for(CapturedProduct capturedProduct : capturedProductList){
            if(!productDal.findProductById(capturedProduct.getId()).isAvailable()){
                expiredProductList.add(capturedProduct.getId());
            }
        }
        ApiResponse apiResponse = new ApiResponse();
        if(!expiredProductList.isEmpty()){
            apiResponse.setSuccessful(false);
            apiResponse.setMessage("Some of the products are expired.");
            apiResponse.setObject(expiredProductList);
            return apiResponse;
        }

        long totalPrice = 0L;
        for(CapturedProduct capturedProduct: capturedProductList){
            Product product = productDal.findProductById(productDal.findProductById(capturedProduct.getId()).getParentId());
            totalPrice += product.getPrice() * capturedProduct.getProductQuantity();
        }

        personalDetail.setCapturedProductList(capturedProductList);
        personalDetail.setDataCreated(System.currentTimeMillis());
        personalDetail.setTotalPrice(totalPrice);
        apiResponse.setSuccessful(true);
        apiResponse.setObject(personalDetailDal.save(personalDetail));
        return apiResponse;
    }

    @Override
    public void savePersonalDetail(PersonalDetail personalDetail) throws Exception {
        personalDetailDal.save(personalDetail);
    }


    @Override
    public PersonalDetail findPersonalDetailById(String id) throws Exception {
        return personalDetailDal.findPersonalDetailById(id);
    }

    @Override
    public void savePersonalDetail(PersonalDetail personalDetail, HttpServletRequest httpServletRequest) throws Exception {
        String token = getTokenService.getTokenFromCookie(httpServletRequest);

        PersonalDetail databasePersonalDetail = personalDetailDal.findPersonalDetailById(getTokenService.getJwtFromCookie(token));

        if (databasePersonalDetail == null) {
            throw new Exception("Personal Detail could not found");
        }
        databasePersonalDetail.setUserEmail(personalDetail.getUserEmail());
        databasePersonalDetail.setUserFullName(personalDetail.getUserFullName());
        databasePersonalDetail.setUserContactNumber(personalDetail.getUserContactNumber());
        databasePersonalDetail.setUserAcceptPrivacyNotice(true);
        databasePersonalDetail.setUserAcceptEmailPromos(personalDetail.isUserAcceptEmailPromos());
        databasePersonalDetail.setShippingDetail(personalDetail.getShippingDetail());
        databasePersonalDetail.setCurrentStatus(StatusEnum.CAPTURED_PERSONAL_DATA_PAYMENT_PROCESS.getStatus());
        personalDetailDal.save(databasePersonalDetail);

    }
}
