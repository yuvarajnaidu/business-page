package com.business.backend.service;

import com.business.backend.model.PaymentIntentModel;
import com.business.backend.model.StripeRequest;

import javax.servlet.http.HttpServletRequest;


public interface StripeService {

    StripeRequest requestPaymentToken(HttpServletRequest httpServletRequest) throws Exception;
    void validatePaymentIntent(PaymentIntentModel paymentIntentModel,HttpServletRequest httpServletRequest) throws Exception;
}
