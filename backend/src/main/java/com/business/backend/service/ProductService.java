package com.business.backend.service;




import com.business.backend.model.ApiResponse;
import com.business.backend.model.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {

    ApiResponse saveProduct(Product product) throws Exception;
    Product findProductById(String id) throws Exception;
    Map<String, Object> getAllProducts() throws Exception;
    List<Product> findProductByAvailable(boolean available) throws Exception;

}
