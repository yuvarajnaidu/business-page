package com.business.backend.service.impl;


import com.business.backend.model.PersonalDetail;
import com.business.backend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class SecurityService {

    @Autowired
    private OrderService orderService;

    public PersonalDetail loadPersonalDetailById(String token) throws Exception {
        try {

            PersonalDetail personalDetail = orderService.findPersonalDetailById(token);

            if (null == personalDetail) {
                throw new Exception("Personal Detail Not Found");
            }

            return personalDetail;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Token Not Found");
        }
    }
}
