package com.business.backend.service.impl;

import com.business.backend.configuration.JwtConfiguration;
import com.business.backend.security.JwtTokenProvider;
import com.business.backend.service.GetTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;


@Service
public class GetTokenServiceImpl implements GetTokenService {

    @Autowired
    private JwtConfiguration jwtConfiguration;

    @Autowired
    private JwtTokenProvider tokenProvider;
    @Override
    public String getTokenFromCookie(HttpServletRequest httpServletRequest) throws Exception {
        if (null != httpServletRequest.getCookies() && httpServletRequest.getCookies().length > 0) {
            for (int i = 0; i < httpServletRequest.getCookies().length; i++) {
                if (jwtConfiguration.getCookieName().equals(httpServletRequest.getCookies()[i].getName()))
                    return httpServletRequest.getCookies()[i].getValue();
            }
        }
        return null;
    }

    @Override
    public String getJwtFromCookie(String token) throws Exception {
        String jwt = null;
        if (StringUtils.hasText(token) && token.startsWith(jwtConfiguration.getPrefix())) {
            jwt = token.substring(jwtConfiguration.getPrefix().length());
        }
        return tokenProvider.getIdFromToken(jwt);
    }
}
