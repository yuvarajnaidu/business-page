package com.business.backend.service.impl;


import com.business.backend.dal.ProductDal;
import com.business.backend.model.ApiResponse;
import com.business.backend.model.Product;
import com.business.backend.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDal productDal;

    Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);


    @Override
    public ApiResponse saveProduct(Product product) throws Exception {
        ApiResponse apiResponse = new ApiResponse();

        try {
            productDal.save(product);
            apiResponse.setSuccessful(true);
            apiResponse.setMessage("Product Created");
            return apiResponse;
        } catch (Exception e) {
            apiResponse.setSuccessful(false);
            apiResponse.setMessage("Please Contact Support");
            logger.error(e.getMessage());
            return apiResponse;
        }
    }

    @Override
    public Product findProductById(String id) throws Exception {
        return productDal.findProductById(id);
    }

    @Override
    public Map<String, Object> getAllProducts() throws Exception {
        List<Product> productList = findProductByAvailable(true);
        Map<String, Object> productMainLevel = new HashMap<>();
        Map<String, List<Object>> productSecondLevel = new HashMap<>();
        Map<String, Object> refactoredMap = new HashMap<>();
        for (Product mainLevel : productList) {
            if (null == mainLevel.getParentId()) {
                productMainLevel.putIfAbsent(mainLevel.getId(), mainLevel);
            }
        }

        productMainLevel.forEach((k, v) -> {
            productSecondLevel.putIfAbsent(k, new ArrayList<>());
            for (Product child : productList) {
                if (null != child.getParentId()) {
                    if (child.getParentId().equals(k)) {
                        productSecondLevel.get(k).add(child);
                    }
                }
            }
        });

        refactoredMap.put("firstLevel", productMainLevel);
        refactoredMap.put("child", productSecondLevel);


        return refactoredMap;
    }

    @Override
    public List<Product> findProductByAvailable(boolean available) throws Exception {
        return productDal.findProductByAvailable(available);
    }

}
