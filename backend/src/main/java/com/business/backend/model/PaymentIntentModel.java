package com.business.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Document(collection = "payment_intents")
public class PaymentIntentModel implements Serializable {

    @Column(name = "payment_intent")
    @JsonProperty("payment_intent")
    String paymentIntent;
    @Column(name = "payment_intent_client_secret")
    @JsonProperty("payment_intent_client_secret")
    String paymentIntentClientSecret;
    @Column(name = "redirect_status")
    @JsonProperty("redirect_status")
    String redirectStatus;
    @Column(name = "personal_detail_id")
    String personalDetailId;

    public String getPaymentIntent() {
        return paymentIntent;
    }

    public void setPaymentIntent(String paymentIntent) {
        this.paymentIntent = paymentIntent;
    }

    public String getPaymentIntentClientSecret() {
        return paymentIntentClientSecret;
    }

    public void setPaymentIntentClientSecret(String paymentIntentClientSecret) {
        this.paymentIntentClientSecret = paymentIntentClientSecret;
    }

    public String getRedirectStatus() {
        return redirectStatus;
    }

    public void setRedirectStatus(String redirectStatus) {
        this.redirectStatus = redirectStatus;
    }

    public String getPersonalDetailId() {
        return personalDetailId;
    }

    public void setPersonalDetailId(String personalDetailId) {
        this.personalDetailId = personalDetailId;
    }
}
