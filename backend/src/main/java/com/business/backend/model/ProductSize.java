package com.business.backend.model;

import javax.persistence.Column;

public class ProductSize {

    @Column(name = "product_size_label")
    private String productSizeLabel;

    @Column(name = "product_size_code")
    private String productSizeCode;

    public String getProductSizeLabel() {
        return productSizeLabel;
    }

    public void setProductSizeLabel(String productSizeLabel) {
        this.productSizeLabel = productSizeLabel;
    }

    public String getProductSizeCode() {
        return productSizeCode;
    }

    public void setProductSizeCode(String productSizeCode) {
        this.productSizeCode = productSizeCode;
    }
}
