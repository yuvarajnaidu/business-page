package com.business.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class CartItem implements Serializable {
    @Column(name = "product_id")
    private String productId;

    @Column(name = "product_index")
    private String productItemIndex;

    @Column(name = "product_quantity")
    private Integer productQuantity;

    @Column(name = "product_size")
    private ProductSize productSize;

    @Column(name = "product_colour")
    private ProductColor productColor;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductItemIndex() {
        return productItemIndex;
    }

    public void setProductItemIndex(String productItemIndex) {
        this.productItemIndex = productItemIndex;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public ProductSize getProductSize() {
        return productSize;
    }

    public void setProductSize(ProductSize productSize) {
        this.productSize = productSize;
    }

    public ProductColor getProductColor() {
        return productColor;
    }

    public void setProductColor(ProductColor productColor) {
        this.productColor = productColor;
    }
}
