package com.business.backend.model;

import com.business.backend.capturedModel.CapturedProduct;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Document(collection = "personal_details")
public class PersonalDetail implements Serializable {

    @Id
    private String id;

    @Column(name = "user_email")
    private String userEmail;

    @Column(name = "user_full_name")
    private String userFullName;

    @Column(name = "user_contact_number")
    private String userContactNumber;

    @Column(name = "user_accept_email_promos")
    private boolean userAcceptEmailPromos;

    @Column(name = "user_accept_privacy_notice")
    private boolean userAcceptPrivacyNotice;

    @Column(name = "shipping_details_id")
    private String shippingDetailsId;

    @Column(name = "shipping_details")
    private ShippingDetail shippingDetail;

    @Column(name = "payment_details_id")
    private String paymentDetailsId;

    @Column(name = "payment_details")
    private PaymentDetail paymentDetail;

    @Column(name = "stripe_request_id")
    private String stripeRequestId;

    @Column(name = "stripe_request")
    private StripeRequest stripeRequest;

    @Column(name = "payment_intent_id")
    private String paymentIntentId;

    @Column(name = "payment_intent")
    private PaymentIntentModel paymentIntentModel;

    @Column(name = "product_list")
    private List<CapturedProduct> capturedProductList;

    @Column(name = "total_price")
    private long totalPrice;

    @Column(name = "current_status")
    private String currentStatus;

    @Column(name = "token")
    private String token;

    @Column(name = "data_created")
    private long dataCreated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserContactNumber() {
        return userContactNumber;
    }

    public void setUserContactNumber(String userContactNumber) {
        this.userContactNumber = userContactNumber;
    }

    public boolean isUserAcceptEmailPromos() {
        return userAcceptEmailPromos;
    }

    public void setUserAcceptEmailPromos(boolean userAcceptEmailPromos) {
        this.userAcceptEmailPromos = userAcceptEmailPromos;
    }

    public boolean isUserAcceptPrivacyNotice() {
        return userAcceptPrivacyNotice;
    }

    public void setUserAcceptPrivacyNotice(boolean userAcceptPrivacyNotice) {
        this.userAcceptPrivacyNotice = userAcceptPrivacyNotice;
    }

    public String getShippingDetailsId() {
        return shippingDetailsId;
    }

    public void setShippingDetailsId(String shippingDetailsId) {
        this.shippingDetailsId = shippingDetailsId;
    }

    public ShippingDetail getShippingDetail() {
        return shippingDetail;
    }

    public void setShippingDetail(ShippingDetail shippingDetail) {
        this.shippingDetail = shippingDetail;
    }

    public String getPaymentDetailsId() {
        return paymentDetailsId;
    }

    public void setPaymentDetailsId(String paymentDetailsId) {
        this.paymentDetailsId = paymentDetailsId;
    }

    public PaymentDetail getPaymentDetail() {
        return paymentDetail;
    }

    public void setPaymentDetail(PaymentDetail paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public List<CapturedProduct> getCapturedProductList() {
        return capturedProductList;
    }

    public void setCapturedProductList(List<CapturedProduct> capturedProductList) {
        this.capturedProductList = capturedProductList;
    }

    public long getDataCreated() {
        return dataCreated;
    }

    public void setDataCreated(long dataCreated) {
        this.dataCreated = dataCreated;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStripeRequestId() {
        return stripeRequestId;
    }

    public void setStripeRequestId(String stripeRequestId) {
        this.stripeRequestId = stripeRequestId;
    }

    public StripeRequest getStripeRequest() {
        return stripeRequest;
    }

    public void setStripeRequest(StripeRequest stripeRequest) {
        this.stripeRequest = stripeRequest;
    }

    public String getPaymentIntentId() {
        return paymentIntentId;
    }

    public void setPaymentIntentId(String paymentIntentId) {
        this.paymentIntentId = paymentIntentId;
    }

    public PaymentIntentModel getPaymentIntentModel() {
        return paymentIntentModel;
    }

    public void setPaymentIntentModel(PaymentIntentModel paymentIntentModel) {
        this.paymentIntentModel = paymentIntentModel;
    }

    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
}
