package com.business.backend.model;

import javax.persistence.Column;

public class ProductColor {


    @Column(name = "product_color_label")
    private String productColorLabel;

    @Column(name = "product_color_code")
    private String productColorCode;

    public String getProductColorLabel() {
        return productColorLabel;
    }

    public void setProductColorLabel(String productColorLabel) {
        this.productColorLabel = productColorLabel;
    }

    public String getProductColorCode() {
        return productColorCode;
    }

    public void setProductColorCode(String productColorCode) {
        this.productColorCode = productColorCode;
    }
}
