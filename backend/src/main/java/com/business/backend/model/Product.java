package com.business.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
@Document(collection = "products")
public class Product implements Serializable {
    @Id
    private String id;

    @Column(name = "shirt_label")
    private String shirtLabel;

    @Column(name = "price")
    private long price;

    @Column(name = "cut_off_price")
    private long cutOffPrice;

    @Column(name = "image")
    private String image;

    @Column(name = "description")
    private String description;

    @Column(name = "product_color")
    private ProductColor productColor;

    @Column(name = "product_image")
    private String productImage;

    @Column(name = "product_size")
    private List<ProductSize> productSizes;

    @Column(name = "parent_id")
    private String parentId;

    @Column(name = "available")
    private boolean available;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShirtLabel() {
        return shirtLabel;
    }

    public void setShirtLabel(String shirtLabel) {
        this.shirtLabel = shirtLabel;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getCutOffPrice() {
        return cutOffPrice;
    }

    public void setCutOffPrice(long cutOffPrice) {
        this.cutOffPrice = cutOffPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductColor getProductColor() {
        return productColor;
    }

    public void setProductColor(ProductColor productColor) {
        this.productColor = productColor;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public List<ProductSize> getProductSizes() {
        return productSizes;
    }

    public void setProductSizes(List<ProductSize> productSizes) {
        this.productSizes = productSizes;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
