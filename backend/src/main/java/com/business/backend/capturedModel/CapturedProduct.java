package com.business.backend.capturedModel;

import com.business.backend.model.ProductColor;
import com.business.backend.model.ProductSize;
import org.springframework.data.annotation.Id;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.List;

public class CapturedProduct implements Serializable {

    private String id;

    private ProductColor productColor;

    private ProductSize productSizes;

    private Integer productQuantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductColor getProductColor() {
        return productColor;
    }

    public void setProductColor(ProductColor productColor) {
        this.productColor = productColor;
    }

    public ProductSize getProductSizes() {
        return productSizes;
    }

    public void setProductSizes(ProductSize productSizes) {
        this.productSizes = productSizes;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }
}
