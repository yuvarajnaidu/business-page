package com.business.backend.utils;

public class Constants {
    public static String ddmmyyyyDateFormat = "dd/MM/yyyy";
    public static String LANG_COOKIE = "nedas_dashboard_lang";
    public static String PRODUCT_LOGO = "nedas_dashboard_product_logo";
    public static String CLIENT_LOGO = "nedas_dashboard_client_logo";
    public static String PRODUCT_NAME = "nedas_dashboard_product_name";
    public static String PRODUCT = "nedas_dashboard_product";
    public static String PRODUCT_SECURE = "nedas_dashboard_product_token";
    public static String CLAIM_AUTHORITIES = "authorities";
    public static String CLAIM_LAST_LOGIN = "last_login";
    public static String PUBLIC_TOKEN = "public_token";

    public static String STRIPE_SUCCEEDED = "succeeded";
    public static String STRIPE_FAILED = "failed";

}
