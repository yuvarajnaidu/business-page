package com.business.backend.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CustomConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/landing/static/**").addResourceLocations("classpath:/templates/landing/static/");
        registry.addResourceHandler("/mycustomfavicon.ico").addResourceLocations("classpath:/templates/");
    }
}