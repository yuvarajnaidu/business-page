import baseService from "./baseService";
export const homeService = {
    getElements,
    requestPaymentToken,
    saveCart,
    validatePaymentIntent,
    saveShippingDetails,
    getProducts,
    authenticateCheckout,
};

function getElements() {
    return baseService.getElements();
}

function getProducts() {
    return baseService.getRequest("/home/getAllProducts");
}

function saveCart(payload) {
    return baseService.postRequest("/home/saveCart", payload);
}

function authenticateCheckout(payload) {
    return baseService.postRequest("/home/authenticateCheckout", payload);
}

function requestPaymentToken() {
    return baseService.postRequest("/checkout/getStripeResponse");
}


function validatePaymentIntent(payload) {
    return baseService.postRequest("/checkout/validatePaymentIntent", payload);
}

function saveShippingDetails(payload) {
    return baseService.postRequest("/checkout/saveShippingDetails", payload);
}