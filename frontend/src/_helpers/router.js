import Vue from 'vue'
import Router from 'vue-router'
import store from '../_store'

Vue.use(Router)

export const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../components/Home')
    },
    {
      beforeEnter: guard,
      path: '/result',
      name: 'result',
      component: () => import('../components/Result')
    },
    {
      path: '/invalidSession',
      name: 'invalidSession',
      component: () => import('../components/InvalidSession.vue')
    },
    {
      beforeEnter: guard,
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('../components/RegionalManager')
    },
    {
      beforeEnter: guard,
      path: '/manage',
      name: 'manage',
      component: () => import('../components/Manage')
    },
    {
      beforeEnter: guard,
      path: '/registration',
      name: 'registration',
      component: () => import('../components/Registration')
    },
    { path: '*', redirect: '/' }
  ]
})

function guard (to, from, next) {
  if (!store.state.home.authenticated) {
    next('/') // allow to enter route
  } else {
    next() // go to '/login';
  }
}
