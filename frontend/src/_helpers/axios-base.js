import axios from 'axios'
import config from '../_config'

export const axiosBase = axios.create({
    baseURL: config.apiUrl(),
    withCredentials: true,
})