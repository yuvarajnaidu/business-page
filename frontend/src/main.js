import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { i18n } from "./_helpers";
import vuetify from "./plugins/vuetify";
import VueFacebookPixel from "vue-analytics-facebook-pixel";
import { ValidationProvider, ValidationObserver, extend } from "vee-validate";
import {
    required,
    email,
    length,
    min,
    max,
    numeric,
} from "vee-validate/dist/rules";
import VueCountdown from "@chenfengyuan/vue-countdown";

Vue.use(VueFacebookPixel);
Vue.analytics.fbq.init("1170275039984371");
Vue.analytics.fbq.event("COTT8N", {
    action: "Landing Page - Land",
});
Vue.component(VueCountdown.name, VueCountdown);

extend("phoneNumberNoZero", {
    validate: (value) => value[0] != 0,
});
extend("required", {...required });
extend("email", {...email });
extend("length", {...length });
extend("min", {...min });
extend("max", {...max });
extend("numeric", {...numeric });

Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);
Vue.config.productionTip = false;

export const mixIn = {
    methods: {
        $translatedErrorMessages: function(errors, name) {
            if (errors && Object.keys(errors).length > 0) {
                for (let [key] of Object.entries(errors)) {
                    return this.$t(`msg.${name}_error_${key}`.toString().toLowerCase());
                }
            }
        },
    },
};

Vue.mixin(mixIn);
new Vue({
    router,
    i18n,
    store,
    vuetify,
    render: (h) => h(App),
}).$mount("#app");