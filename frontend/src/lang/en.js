export const en = {
    msg: {
        full_name_label: "Full Name",
        fullname_error_required: "Full Name Required",

        email_label: "Email",
        email_error_required: "Email Required",
        email_error_email: "Please enter a valid email",

        contact_number_label: "Contact Number",
        contactnumber_error_required: "Contact Number Required",
        contactnumber_error_phonenumbernozero: "Skip first digit of phone number '0'",
        contactnumber_error_min: "Please Enter Valid Phone Number",
        contactnumber_error_max: "Please Enter Valid Phone Number",
        contactnumber_error_numeric: "Please Enter Valid Phone Number",

        address_line_1_label: "Address Line 1",
        addressline1_error_required: "Address Line 1 Required",

        address_line_2_label: "Address Line 2",

        postal_code_label: "Postal Code",
        postalcode_error_required: "Postal Code Required",
        postalcode_error_length: "Please Enter Valid Postal Code",
        postalcode_error_numeric: "Please Enter Valid Postal Code",

        cities_label: "City",
        cities_error_required: "City Required",

        state_label: "State",
        state_error_required: "State Required",

        colour_label: "Colour",
        colour_error_required: "Please Choose your shirt Colour",

        size_label: "Size",
        size_error_required: "Please Choose your shirt Size",
    },
};