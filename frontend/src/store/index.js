import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        sideBarToggle: false,
        count: 1,
        modalDialog: false,
        message: "",
        indexToRemove: null,
        stripeToken: null,
        token: "",
        form: {},
        sessionTimeout: false,
        overlayLoader: false,
        cart: [
            //     {
            //     label: "Pulingo",
            //     price: 1099,
            //     cutOffPrice: 4099,
            //     id: 111,
            //     count: 1,
            //     size: "M",
            //     image: "modelHeader.jpg",
            //     sizeCode: "m",
            //     color: "Yellow",
            //     colorCode: "yellow",
            // },
        ],
    },
    mutations: {
        FORM_ACTION(state, payload) {
            state = Object.assign(state, payload);
        },
        SIDEBAR_TOGGLE(state) {
            state.sideBarToggle = !state.sideBarToggle;
        },
        TOGGLE_MODAL_DIALOG(state, payload) {
            state.modalDialog = payload;
        },
        SET_STRIPE_TOKEN(state, payload) {
            state.stripeToken = payload;
        },
        ADD_CART(state, payload) {
            state.cart.push(payload);
        },
        CLEAR_CART(state) {
            state.cart = [];
        },
        INCREASE_COUNT(state, payload) {
            state.cart[payload].productQuantity++;
        },
        DECREASE_COUNT(state, payload) {
            if (state.cart[payload].productQuantity > 1) {
                state.cart[payload].productQuantity--;
            }
        },
        OVERLAY_LOADER(state) {
            state.overlayLoader = !state.overlayLoader;
        },
    },
    actions: {
        setFormAction({ commit }, payload) {
            commit("FORM_ACTION", payload);
        },
        createSideBarToggle({ commit }, event) {
            commit("SIDEBAR_TOGGLE", event);
        },
        setStripeToken({ commit }, payload) {
            commit("SET_STRIPE_TOKEN", payload);
        },
        addCard({ commit }, payload) {
            commit("ADD_CART", payload);
        },
        clearCart({ commit }, event) {
            commit("CLEAR_CART", event);
        },
        decreaseCount({ commit }, payload) {
            commit("DECREASE_COUNT", payload);
        },
        increaseCount({ commit }, payload) {
            commit("INCREASE_COUNT", payload);
        },
        overlayLoader({ commit }, event) {
            commit("OVERLAY_LOADER", event);
        },
    },
    modules: {},
    getters: {
        count: (state) => {
            return state.count;
        },
        getSideBarToggleStatus: (state) => {
            return state.sideBarToggle;
        },
        getStripeToken: (state) => {
            return state.stripeToken;
        },
        getCart: (state) => {
            return state.cart;
        },
        getModalDialog: (state) => {
            return state.modalDialog;
        },
    },
});