// vue.config.js
module.exports = {
    // proxy all webpack dev-server requests starting with /api
    // to our Spring Boot backend (localhost:8088) using http-proxy-middleware
    // see https://cli.vuejs.org/config/#devserver-proxy
    devServer: {
        port: 8020,
        proxy: {
            "/api": {
                target: "http://localhost:8090", // this configuration needs to correspond to the Spring Boot backends' application.properties server.port
                // ws: true,
                changeOrigin: true,
            },
        },
    },
    chainWebpack: (config) => {
        config.plugin("html").tap((args) => {
            args[0].title = "COTT8N";
            return args;
        });
    },
    // Change build paths to make them Maven compatible
    // see https://cli.vuejs.org/config/
    configureWebpack: {
        devtool: "source-map",
    },
    outputDir: "target/dist",
    assetsDir: "landing/static",
    indexPath: "landing_index.html",
    transpileDependencies: ["vuetify"],
};